
var NOW = new Date();
var DAYS = ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."];
var MONTHS = ["janv.", "fév.", "mars", "avril", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."];
var ONE_DAY = 1000 * 60 * 60 * 24;

// Date.isThisYear() function
if (typeof Date.isThisYear !== 'function') {
    
    /**
     * Return true if current date is this year
     * @returns {Boolean}
     */
    Date.prototype.isThisYear = function() {
        return this.getFullYear() === NOW.getFullYear();
    };
}

// Date.isThisMonth() function
if (typeof Date.isThisMonth !== 'function') {
    
    /**
     * Return true if current date is this mouth
     * @returns {Boolean}
     */
    Date.prototype.isThisMonth = function() {
        return this.isThisYear() 
            && this.getMonth() === NOW.getMonth();
    };
}

// Date.isToday() function
if (typeof Date.isToday !== 'function') {
    
    /**
     * Return true if current date is today
     * @returns {Boolean}
     */
    Date.prototype.isToday = function() {
        return this.isThisMonth()
            && this.getDate() === NOW.getDate();
    };
}

// Date.daysBetween(date) function
if (typeof Date.daysBetween !== 'function') {
    
    /**
     * Compare date with another one and return days between
     * @param {Date} date - Date to compare with
     * @returns {Number}
     */
    Date.prototype.daysBetween = function(date) {
        var date1_ms = this.getTime();
        var date2_ms = date.getTime();
        
        var diff_ms = Math.abs(date1_ms - date2_ms);
        return Math.round(diff_ms / ONE_DAY);
        
    };
}

// Date.formatHour() function
if (typeof Date.formatTime !== 'function') {
    
    /**
     * Show time
     * @returns {String}
     */
    Date.prototype.formatTime = function() {
        var hours = new String(this.getHours());
        if (hours.length === 1)
            hours = '0' + hours;
        
        var minutes = new String(this.getMinutes());
        if (minutes.length === 1)
            minutes = '0' + minutes;
        
        return hours + ':' + minutes;
    };
}

// Date.formatDate() function
if (typeof Date.formatDate !== 'function') {
    
    /**
     * Show date
     * @returns {String}
     */
    Date.prototype.formatDate = function() {
        if (this.daysBetween(NOW) < 7)
            return DAYS[this.getDay()];
        
        var date = this.getDate() + ' ' + MONTHS[this.getMonth()];
        if (this.isThisYear())
            return date;
        
        return date + ' ' + this.getFullYear();
    };
}

// Date.formatDateTime() function
if (typeof Date.formatDateTime !== 'function') {
    
    /**
     * Show date and time
     * @returns {String}
     */
    Date.prototype.formatDateTime = function() {        
        var time = this.formatTime();
        if (this.isToday())
            return time;
            
        return this.formatDate() + ' à ' + time;
    };
}
