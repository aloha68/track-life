
var DATABASE_VERSION = 1;       // Current version of database
var DATABASE_ERASE = false;     // Set to true to erase database


/**
 * Class to list all changes in database structure
 * @type type
 */
class TrackLifeDatabaseMigrator {
    
    constructor(db) {
        this._db = db;
        this.onMigrationEnded = null;
        this.onMigrationError = null;
    }
    
    /**
     * Migrate database if needed
     */
    migrate() {
        
        // If devs want to erase database
        if (DATABASE_ERASE) {
            this.dropDatabase();
            return;
        }

        // If database exists, start migration from current version, else start from scratch
        this._db.executeSql('SELECT MAX(id) AS version FROM version', null,
            (result) => this.run(result.rows.item(0).version),
            () => this.run(0)
        );
    }
    
    /**
     * Drop all tables from database
     */
    dropDatabase() {
        
        var sql = this.sqlForDropDatabase();
        Aloha.log('DBMigrator.dropDatabase: ' + sql.length + ' SQL commands');
        
        this._db.sqlBatch(sql,
            () => {
                DATABASE_ERASE = false;
                this.migrate();
            },
            (error) => Aloha.error('DBMigrator.dropDatabase', error.message, this.onMigrationError)
        );
    }
    
    /**
     * Run recursively each version until version == DATABASE_VERSION
     * @param {Number} version - Database version number
     */
    run(version) {
        
        // If current version matches with database version, migration is ended
        if (version >= DATABASE_VERSION) {
            Aloha.log('DBMigrator.run: call DBMigrator.onMigrationEnded event');
            Aloha.callFunction(this.onMigrationEnded);
            return;
        }
        
        // Else, we want to install next version
        version += 1;
        
        // Get SQL commands for this version
        var sql = this.getSqlForVersion(version);
        if (sql === null || sql.length <= 0)
            return;
        
        // Add SQL to insert current version in database
        sql.push(['INSERT INTO version (id, date) VALUES (?,?)', [version, new Date().getTime()]]);
        
        Aloha.log('DBMigrator.run(' + version + '): ' + sql.length + ' SQL commands');
        
        this._db.sqlBatch(sql,
            () => this.run(version + 1),
            (error) => Aloha.error('DBMigrator.run(' + version + ')', error.message, this.onMigrationError)
        );
    }
    
    /**
     * Return all SQL commands for a specific version
     * @param {Number} version
     * @returns {Array}
     */
    getSqlForVersion(version) {
        
        // Build function name from version
        var functionName = 'sqlForV' + version;
        
        try {
            // Try to execute function
            return this[functionName]();
        } catch (e) {
            return [];
        }
    }
    
    /**
     * SQL for drop database
     * @returns {Array}
     */
    sqlForDropDatabase() {
        return  [
            'DROP TABLE IF EXISTS activity',
            'DROP TABLE IF EXISTS version'
        ];
    }
    
    /**
     * SQL for DBv1
     * @returns {Array}
     */
    sqlForV1() {
        return [
            'CREATE TABLE version (id INTEGER, date INTEGER)',
            'CREATE TABLE activity (' +
                'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
                'title TEXT, ' +
                'timestamp INTEGER)'
        ];
    }
}


/**
 * Database connector for TrackLife application
 * Must be construct when DEVICEREADY event or after
 * @type TrackLifeDatabase
 */
class TrackLifeDatabase {
    
    /**
     * Database constructor
     */
    constructor() {
        this._db = null;
        this.onDatabaseReady = null;
        this.onDatabaseError = null;
    }
    
    /**
     * Initialize database
     */
    initialize() {
        Aloha.debug('DB.initialize');
        
        this._db = window.sqlitePlugin.openDatabase(
            { name: 'TrackLife.db', location: 'default' },
            (db) => {
                Aloha.log('DB.initialize: connection to database OK');
                
                // Migrate database
                var migrator = new TrackLifeDatabaseMigrator(db);
                migrator.onMigrationEnded = this.onDatabaseReady;
                migrator.onMigrationError = this.onDatabaseError;
                migrator.migrate();
            },
            (error) => Aloha.error('DB.initialize', JSON.stringify(error), this.onDatabaseError)
        );
    }
    
    /**
     * Get all activities from database
     * @param {function} success - Success callback
     */
    getActivities(success) {
        Aloha.debug('DB.getActivities');
        
        this.runSql('Get all activities',
            'SELECT id, title, timestamp FROM activity ORDER BY timestamp DESC', [],
            (res) => {
                Aloha.log('DB.getActivities: ' + res.rows.length + ' activities found');

                var activities = [];
                for (var i = 0; i < res.rows.length; i++) {

                    var item = res.rows.item(i);
                    var act = new Activity(item.title, new Date(item.timestamp));
                    act.id = item.id;

                    activities.push(act);
                }
                Aloha.log('DB.getActivities: call anonymous event');
                Aloha.callFunction(success, activities);
            }
        );
    }
    
    /**
     * Get distinct titles of all activities
     * @param {type} success
     */
    getDistinctActivitiesTitle(success) {
        Aloha.debug('DB.getDistinctActivitiesTitle');
        
        this.runSql('Get distinct activities title',
            'SELECT DISTINCT title FROM activity ORDER BY title', [],
            (res) => {
                Aloha.log('DB.getDistinctActivitiesTitle: ' + res.rows.length + ' distinct titles found');
                
                var titles = [];
                for (var i = 0; i < res.rows.length; i++)
                    titles.push(res.rows.item(i).title);
                
                Aloha.log('DB.getDistinctActivitiesTitle: call anonymous event');
                Aloha.callFunction(success, titles);
            }
        );
    }
    
    /**
     * Add an activity
     * @param {Activity} activity
     * @param {function} success - Success callback
     */
    addActivity(activity, success) {
        Aloha.debug('DB.addActivity');
        
        if (!(activity instanceof Activity))
            return;
        
        this.runSql(
            'Add an activity',
            "INSERT INTO activity (title, timestamp) VALUES (?,?)",
            [activity.title, activity.timestamp],
            (res) => {
                Aloha.log('DB.addActivity: activity added n°' + res.insertId);
                activity.id = res.insertId;
                Aloha.log('DB.addActivity: call anonymous event');
                Aloha.callFunction(success, activity);
            }
        );
    }
    
    goodRequest(success) {
        this.runSql('Good request', 'SELECT MAX(id) AS version FROM version', [], 
            (res) => {
                Aloha.callFunction(success, res.rows.item(0).version);
            }
        );
    }
    
    wrongRequest(success) {
        this.runSql('Wrong request', 'SELECT id FROM abcde', [], success);
    }
    
    /**
     * Run SQL with values escaping then run success callback with res as parameter
     * @param {String} context
     * @param {String} sql
     * @param {Array} values
     * @param {function} success
     */
    runSql(context, sql, values, success) {
        
        var number = ((Math.floor(Math.random() * 10000) + 1) + '').padStart(4, '0');
        var logPrefix = 'DB.SQL[' + number + '] ';
        
        Aloha.debug(logPrefix + context);
        Aloha.debug(logPrefix + sql);
        
        if (values && values.length > 0)
            Aloha.debug(logPrefix + ' with values: ' + values)
        
        this._db.transaction(
            (tx) => {
                tx.executeSql(sql, values,
                    (tx, res) => {
                        Aloha.callFunction(success, res);
                    },
                    (tx, err) => Aloha.error(logPrefix + 'request KO', err, this.onDatabaseError)
                );
            },
            (err) => Aloha.error(logPrefix + 'transaction KO', err, this.onDatabaseError)
            //() => {
            //    Aloha.debug(logPrefix + 'transaction OK')
            //}
        );
    }
}
