
/**
 * Model for an activity
 * @type Activity
 */
class Activity {
    
    /**
     * Constructor
     * @param {String} title
     * @param {Date} date
     * @returns {Activity}
     */
    constructor(title, date) {
        
        if (!(date instanceof Date))
            throw new TypeError('Param "date" has to be Date type');
        
        this.id = 0;
        this._title = title;
        this._date = date;
    }
    
    get title() {
        return this._title;
    }
    
    get date() {
        return this._date.formatDateTime();
    }
    
    get timestamp() {
        return this._date.getTime();
    }
}