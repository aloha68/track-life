/*
 * Débuggage sur android: adb logcat -v long chromium:D SystemWebViewClient:D *:S
 */


/**
 * Back-end class
 * @type type
 */
class App {
    
    constructor() {
        console.log('App started');
        
        this.db = null;     // Database connector
        this.view = null;   // HTML view
        
        // Some database caches
        this._activities = [];
        this._distinctTitles = [];
    }
    
    /**
     * Initialize application
     */
    initialize() {
        Aloha.log('App initialize...');
        
        this.view = new View();
        this.view.onDeviceReady = () => this.start();
        this.view.onNewActivity = (title) => this.saveNewActivity(title);
        
        this.view.initialize();
        
        Aloha.log('App initialized!');
    }
    
    /**
     * Event called when device is ready
     */
    start() {
        try {
            Aloha.raw('(¯`·._.··¸.-~*´¨¯¨`*·~-.,-( Welcome on ' + BuildInfo.displayName + ' v' + BuildInfo.version + ' ) -,.-~*´¨¯¨`*·~-.¸··._.·´¯)');
            Aloha.log('App start...');

            // Init database
            this.db = new TrackLifeDatabase();
            this.db.onDatabaseReady = () => this.databind();
            this.setErrorContext('Init database');
            this.db.initialize();
            
            this.view.start();
            
            Aloha.log('App started!');
        } catch (e) {
            console.trace();
            Aloha.handleException(e);
        }
    }
    
    /**
     * Fetch and bind data to the view
     */
    databind() {
        try {
            Aloha.log("App.databind");
            
            // Get activities from database
            this.setErrorContext('Get activities from database');
            this.db.getActivities((res) => this.retrieveActivities(res));

            // Add auto completion on add activity popup
            this.setErrorContext('Get distinct activities title from database');
            this.db.getDistinctActivitiesTitle((res) => this.retrieveDistinctTitles(res));
            
        } catch (e) {
            console.trace();
            Aloha.handleException(e);
        }
    }
    
    /**
     * Retrieve all activities callback
     * @param {Array} activities
     */
    retrieveActivities(activities) {
        try {
            Aloha.log('App.retrieveActivities');
            
            this._activities = activities;
            this.view.setAllActivities(activities);
        } catch (e) {
            console.trace();
            Aloha.handleException(e);
        }
    }
    
    /**
     * Retrieve all distinct titles callback
     * @param {Array} titles
     */
    retrieveDistinctTitles(titles) {
        try {
            Aloha.log('App.retrieveDistinctTitles');
            
            this._distinctTitles = titles;
            this.view.setAutocompleteSource(titles);
        } catch (e) {
            console.trace();
            Aloha.handleException(e);
        }
    }
    
    /**
     * Retrieve new inserted activity callback
     * @param {Activity} activity
     */
    retrieveNewActivity(activity) {
        try {
            Aloha.log('App.retrieveNewActivity');
            
            if (!(activity instanceof Activity))
                throw new TypeError('activity must be an Activity');
            
            if (this._distinctTitles.indexOf(activity.title) < 0) {
                this._distinctTitles.push(activity.title);
                this.view.setAutocompleteSource(this._distinctTitles);
            }
            
            this.view.addNewActivity(activity);
        } catch (e) {
            console.trace();
            Aloha.handleException(e);
        }
    }
    
    /**
     * Define a context to show next errors
     * @param {String} context
     */
    setErrorContext(context) {
        this.db.onDatabaseError = (error) => this.view.showError(error, context);
    }
    
    /**
     * Save a new activity
     * @param {Activity} title
     */
    saveNewActivity(title) {
        try {
            Aloha.log('App.saveNewActivity - ' + title);

            var activity = new Activity(title, new Date());

            this.setErrorContext('Add an activity in database');
            this.db.addActivity(activity, (act) => this.retrieveNewActivity(act));
            
        } catch (e) {
            console.trace();
            Aloha.handleException(e);
        }
    }
}

var app = new App();
app.initialize();
