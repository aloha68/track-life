
/**
 * Logger de l'application
 * Débuggage sur android: adb logcat -v long chromium:D SystemWebViewClient:D *:S
 * @type Logger
 */
class Logger {
    
    /**
     * Add raw message
     * @param {String} message
     */
    raw(message) {
        console.log(message);
    }
    
    /**
     * Add message log
     * @param {string} type
     * @param {string} message
     */
    log(type, message) {
        
        if (message === undefined) {
            message = type;
            type = "LOG";
        }
        this.raw(type + " " + message);
    }
    
    /**
     * Add INFO level message
     * @param {type} message
     */
    info(message) {
        this.log("INF", message);
    }
    
    /**
     * Add ERROR level message
     * @param {string} message
     * @param {string} error
     * @param {function} callback
     */
    error(message, error, callback) {
        
        if (error === undefined) {
            this.log("ERR", message);
        } else {
            this.log("ERR", message + ": " + error);
        }
        
        if (typeof callback === 'function')
            callback(error);
    }
}

var logger = new Logger();