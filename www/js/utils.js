
/********************
** ALOHA FUNCTIONS **
********************/

class Aloha {
    
    /**
     * Add raw message
     * @param {String} message
     */
    static raw(message) {
        try {
            console.log(message);
        } catch (e) {
            console.log('/!\ EXCEPTION on Aloha namespace !!!');
            console.log('    ' + typeof(e) + ': ' + e.message);
        }
    }
    
    /**
     * Add message log
     * @param {String} type
     * @param {String} message
     */
    static log(type, message) {
        try {
            if (message === undefined) {
                message = type;
                type = 'LOG';
            }
            Aloha.raw(type + ' ' + message);
        } catch (e) {
            console.log('/!\ EXCEPTION on Aloha namespace !!!');
            console.log('    ' + typeof(e) + ': ' + e.message);
        }
    }
    
    /**
     * Add ERROR level message
     * @param {String} message
     * @param {String} error
     * @param {function} callback
     */
    static error(message, error, callback) {
        try {
            if (error instanceof String || typeof error === 'string')
                message += ': ' + error;
            else if (error instanceof Error)
                message += ': ' + error.message + ' (' + typeof(error) + ')';
            else
                error = message;

            console.error('ERR ' + message);

            if (Aloha.isFunction(callback))
                Aloha.callFunction(callback, error);
            
        } catch (e) {
            console.log('/!\ EXCEPTION on Aloha namespace !!!');
            console.log('    ' + typeof(e) + ': ' + e.message);
        }
    }
    
    /**
     * Add debug message
     * @param {String} message
     */
    static debug(message) {
        try {
            Aloha.log('DBG', message);
        } catch (e) {
            console.log('/!\ EXCEPTION on Aloha namespace !!!');
            console.log('    ' + typeof(e) + ': ' + e.message);
        }
    }
    
    /**
     * Handle exception
     * @param {type} ex
     */
    static handleException(ex) {
        try {
            Aloha.raw('/!\ EXCEPTION ' + typeof(ex) + ': ' + ex.message);
        } catch (e) {
            console.log('/!\ EXCEPTION on Aloha namespace !!!');
            console.log('    ' + typeof(e) + ': ' + e.message);
        }
    }
    
    /**
     * Return true if callback is callable
     * @param {function} callback
     * @returns {Boolean}
     */
    static isFunction(callback) {
        return callback && typeof callback === 'function';
    }
    
    /**
     * Call function callback with all others parameters
     * Return true is callback is fired
     * @param {function} callback
     * @returns {Boolean}
     */
    static callFunction(callback) {
        try {
            if (!Aloha.isFunction(callback)) {
                Aloha.debug('Aloha.callFunction: Uncallable object: ' + callback);
                return;
            }

            Aloha.debug(
                'Aloha.callFunction: ' + (callback.name || 'anonymous') +
                ' with ' + (arguments.length - 1) + ' argument(s)'
            );

            var args = null;
            if (arguments.length > 1) {
                args = Array.prototype.slice.call(arguments, 1);
                //for (var i = 0; i < args.length; i++)
                //    Aloha.debug('  - Arg' + (i + 1) + ': ' + args[i] + ' (' + typeof(args[i]) + ')');
            }

            try {
                callback.apply(this, args);
            } catch (e) {
                Aloha.error('Aloha.callFunction: Error during callback', e);
            }
            
        } catch (e) {
            console.log('/!\ EXCEPTION on Aloha namespace !!!');
            console.log('    ' + typeof(e) + ': ' + e.message);
        }
    }
};

/*********************
** STRING FUNCTIONS **
*********************/

// https://github.com/uxitten/polyfill/blob/master/string.polyfill.js
// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String/repeat
if (!String.prototype.padStart) {
    String.prototype.padStart = function (count, str) {
        return (str || ' ').repeat(count - this.length).substr(0,count) + this;
    };
}

/********************
** DATES FUNCTIONS **
********************/

var NOW = new Date();
var ONE_DAY = 1000 * 60 * 60 * 24;

// Date.isThisYear() function
if (typeof Date.isThisYear !== 'function') {
    
    /**
     * Return true if current date is this year
     * @returns {Boolean}
     */
    Date.prototype.isThisYear = function() {
        return this.getFullYear() === NOW.getFullYear();
    };
}

// Date.isThisMonth() function
if (typeof Date.isThisMonth !== 'function') {
    
    /**
     * Return true if current date is this mouth
     * @returns {Boolean}
     */
    Date.prototype.isThisMonth = function() {
        return this.isThisYear() 
            && this.getMonth() === NOW.getMonth();
    };
}

// Date.isToday() function
if (typeof Date.isToday !== 'function') {
    
    /**
     * Return true if current date is today
     * @returns {Boolean}
     */
    Date.prototype.isToday = function() {
        return this.isThisMonth()
            && this.getDate() === NOW.getDate();
    };
}

// Date.daysBetween(date) function
if (typeof Date.daysBetween !== 'function') {
    
    /**
     * Compare date with another one and return days between
     * @param {Date} date - Date to compare with
     * @returns {Number}
     */
    Date.prototype.daysBetween = function(date) {
        var date1_ms = this.getTime();
        var date2_ms = date.getTime();
        
        var diff_ms = Math.abs(date1_ms - date2_ms);
        return Math.round(diff_ms / ONE_DAY);
        
    };
}

// Date.formatHour() function
if (typeof Date.formatTime !== 'function') {
    
    /**
     * Show time
     * @returns {String}
     */
    Date.prototype.formatTime = function() {
        var hours = new String(this.getHours());
        if (hours.length === 1)
            hours = '0' + hours;
        
        var minutes = new String(this.getMinutes());
        if (minutes.length === 1)
            minutes = '0' + minutes;
        
        return hours + ':' + minutes;
    };
}

// Date.formatDate() function
if (typeof Date.formatDate !== 'function') {
    
    /**
     * Show date
     * @returns {String}
     */
    Date.prototype.formatDate = function() {
        if (this.daysBetween(NOW) < 7)
            return DAYS[this.getDay()];
        
        var date = this.getDate() + ' ' + MONTHS[this.getMonth()];
        if (this.isThisYear())
            return date;
        
        return date + ' ' + this.getFullYear();
    };
}

// Date.formatDateTime() function
if (typeof Date.formatDateTime !== 'function') {
    
    /**
     * Show date and time
     * @returns {String}
     */
    Date.prototype.formatDateTime = function() {        
        var time = this.formatTime();
        if (this.isToday())
            return time;
            
        return this.formatDate() + ' à ' + time;
    };
}

