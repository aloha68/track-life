
/**
 * Front-end class
 * @type type
 */
class View {
    
    constructor() {
        this.onDeviceReady = null;
        this.onNewActivity = null;
    }
    
    /**
     * Initialize view
     */
    initialize() {
        Aloha.log('View initialize...');
        
        if (this.onDeviceReady === null)
            throw new TypeError('View.onDeviceReady must be defined');
        
        document.addEventListener('deviceready', this.onDeviceReady, false);
        
        $('#btnAddActivity').click(() => this.onBtnAddActivityClick());
        $('#btnSaveActivity').click(() => this.onBtnSaveActivityClick());
        
        Aloha.log('View initialized!');
    }
    
    /**
     * Start view
     */
    start() {
        Aloha.log('View start...');
        
        // Disable splash screen
        $('#splash').hide();
            
        Aloha.log('View started!');
    }
    
    /**
     * Event called when btnAddActivity is clicked
     */
    onBtnAddActivityClick() {
        try {
            Aloha.log("View.onBtnAddActivityClick");

            // Show popup
            $('#popupAddActivity').popup('open', { positionTo: '#titleBar' });

            // Input format
            $('#txtTitle')
                .attr('placeholder', this.getRandomActivityTitle())
                .val('')
                .focus();
        
        } catch (e) {
            console.trace();
            Aloha.handleException(e);
        }
    }
    
    /**
     * Event called when btnSaveActivity is clicked
     */
    onBtnSaveActivityClick() {
        try {
            Aloha.log("View.onBtnSaveActivityClick");

            var value = $('#txtTitle').val();
            if (value.length === 0)
                return false;

            Aloha.log("View.onBtnSaveActivityClick: call View.onNewActivity event");
            Aloha.callFunction(this.onNewActivity, value);
            
        } catch (e) { 
            console.trace(); 
            Aloha.handleException(e); 
        }
        
        return false;
    }
    
    /**
     * Create HTML for an activity
     * @param {Activity} activity
     * @returns {Element}
     */
    createActivityHtml(activity) {
        if (!(activity instanceof Activity))
            throw new TypeError('activity must be Activity');
        
        // Create list element
        var li = document.createElement("li");
        li.innerHTML = "<li class=\"activite\">" + 
            " <span class=\"date\">" + activity.date + "</span>" + 
            " <p class=\"title\">" + activity.title + "</p>" + 
            "</li>";
    
        return li;
    }
    
    /**
     * Return random activity title (from RANDOM_PLACEHOLDERS list)
     * @returns {String}
     */
    getRandomActivityTitle() {
        var rdmIndex = Math.floor(Math.random() * RANDOM_PLACEHOLDERS.length);
        return RANDOM_PLACEHOLDERS[rdmIndex];
    }
    
    /**
     * Add a new activity at the top of the list
     * @param {Activity} activity
     */
    addNewActivity(activity) {
        try {
            Aloha.debug('View.addNewActivity');

            $('#popupAddActivity').popup('close');
            $('#activities').prepend(this.createActivityHtml(activity));
        } catch (e) {
            console.trace();
            Aloha.handleException(e);
        }
    }
    
    /**
     * Fill HTML list with list of activities
     * @param {type} activities
     */
    setAllActivities(activities) {
        try {
            Aloha.debug('View.setAllActivities');

            if (activities.length <= 0)
                return;

            // Add activities to html
            var activities_list = $('#activities');
            activities.forEach((act) => {
                activities_list.append(this.createActivityHtml(act));
            });
        } catch (e) {
            console.trace();
            Aloha.handleException(e);
        }
    }
    
    /**
     * Normalize value with remove all accents
     * @param {String} value
     * @returns {String}
     */
    normalize(value) {
        try {
            var res = "";
            for (var i = 0; i < value.length; i++)
                res += ACCENT_MAP[value.charAt(i)] || value.charAt(i);
            return res;
        } catch (e) {
            console.trace();
            Aloha.handleException(e);
        }
    }
    
    /**
     * Set auatocomplete source for activity add
     * @param {type} source
     */
    setAutocompleteSource(source) {
        try {
            Aloha.debug('App.setAutocompleteSource');

            if (source.length <= 0)
                return;

            $('#txtTitle').autocomplete({ 
                source: (request, response) => {
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), 'i');
                    response($.grep(source, (value) => {
                        value = value.label || value.value || value;
                        return matcher.test(value) || matcher.test(this.normalize(value));
                    }));
                }
            });
            
        } catch (e) {
            console.trace();
            Aloha.handleException(e);
        }
    }
    
    /**
     * Display an error with a title
     * @param {string} error - Error
     * @param {string} title - Title of the message box
     */
    showError(error, title) {
        try {
            if (title === undefined)
                title = 'Error';
            navigator.notification.alert(error, null, title);
        } catch (e) {
            console.trace();
            Aloha.handleException(e);
        }
    }
}
