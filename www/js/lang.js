
/**
 * List of random placeholders to put on add-activity text
 * @type Array
 */
var RANDOM_PLACEHOLDERS = [
    "Manger végan",
    "Fumer un pétard (encore...)",
    "Jouer aux cartes",
    "Faire la vaisselle",
    "Aller chez le docteur",
    "Rendre visite à ma grand-mère"
];

/**
 * List of all accentued character and their equivalent without accent
 * @type type
 */
var ACCENT_MAP = {
    'à': 'a',
    'â': 'a',
    'é': 'e',
    'è': 'e',
    'ê': 'e',
    'ë': 'e',
    'î': 'i',
    'ï': 'i',
    'ô': 'o',
    'ö': 'o',
    'ù': 'u'
};

/**
 * Days of the week
 * @type Array
 */
var DAYS = ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."];

/**
 * Months of the year
 * @type Array
 */
var MONTHS = ["janv.", "fév.", "mars", "avril", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."];
